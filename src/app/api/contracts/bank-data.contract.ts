export class BankDataContract {
  email: string;
  dni: string;
  iban: string;
  address: string;
  city: string;
  zipCode: string;
}
