
export class ApiResultContract<T> {
  message: string;
  isError: string;
  result: T;

}
