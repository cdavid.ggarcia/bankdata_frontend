import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TokenContract } from '../../contracts/token.contract';
import { API_URL_LOGIN } from '../../constants/api-urls.constants';
import { map } from 'rxjs/operators';
import { ApiResultContract } from '../../contracts/api-result.contract';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiAuthenticationService {

  constructor(
    private http: HttpClient
  ) { }
  authenticate(email: string, password: string): Observable<TokenContract> {
    return this.http.post(`${ environment.httpBackendUrl }/${ API_URL_LOGIN }`, { email, password })
    .pipe(
      map((response: ApiResultContract<TokenContract>) => response.result)
    );
  }
}
