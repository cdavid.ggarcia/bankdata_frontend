import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BankDataModel } from 'src/app/services/bank-data/models/bank-data.model';
import { BankDataContract } from '../../contracts/bank-data.contract';
import { Observable } from 'rxjs';
import { ApiResultContract } from '../../contracts/api-result.contract';
import { API_URL_USER, API_URL_BANK_DATA } from '../../constants/api-urls.constants';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiBankDataService {

  constructor(
    private http: HttpClient
    ) { }

  setBankData(bankData: BankDataContract): Observable<boolean> {
     return this.http.post(`${ environment.httpBackendUrl }/${ API_URL_USER }/${ API_URL_BANK_DATA }`, { bankData })
    .pipe(
      map((response: ApiResultContract<boolean>) => response.result)
    );
  }

  getBankData(email: string): Observable<BankDataContract> {
    return this.http.get(`${ environment.httpBackendUrl }/${ API_URL_USER }/${ API_URL_BANK_DATA }/${ email }`)
   .pipe(
     map((response: ApiResultContract<BankDataContract[]>) =>
      (response && response.result) ? response.result[0] : null )
   );
 }
}
