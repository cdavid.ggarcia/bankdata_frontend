import { TestBed } from '@angular/core/testing';

import { ApiBankDataService } from './api-bank-data.service';

describe('ApiBankDataService', () => {
  let service: ApiBankDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiBankDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
