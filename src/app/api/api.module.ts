import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiAuthenticationService } from './services/api-authentication/api-authentication.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ApiAuthenticationService
  ]
})
export class ApiModule {}
