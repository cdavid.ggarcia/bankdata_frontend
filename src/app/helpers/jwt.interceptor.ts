import { ACCESS_TOKEN, RESET_TOKEN } from '../constants/auth.constants';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StorageService } from '../services/storage/storage.service';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  jwtHelper = new JwtHelperService();

  constructor(
    private storage: StorageService
    ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.storage.getItem(ACCESS_TOKEN) ? this.storage.getItem(ACCESS_TOKEN) : this.storage.getItem(RESET_TOKEN);

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
    return next.handle(request);
  }
}
