import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';


@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, headers, body } = request;
    // console.log(url, method, headers, body);
    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(
        materialize()
      )
      .pipe(delay(500))
      .pipe(dematerialize());

    function handleRoute() {
      switch (true) {
        // case url.endsWith('/users/authenticate') && method === 'POST':
        //   return authenticate();
        default:
          return next.handle(request);
      }
    }

    // route functions

    // function authenticate() {
    //   const { email, password } = body;

    //   // const us = JSON.parse(localStorage.getItem('users'));
    //   // const user = us.find(x => x.documentNumber === documentNumber && x.password === password);
    //   // if (!user) {
    //   //   console.error('Email or password is incorrect');
    //   // }
    //   return ok({
    //     result: {
    //       // tslint:disable-next-line: max-line-length
    // tslint:disable-next-line: max-line-length
    //       accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
    //       refreshToken: 'GHJREGT68RT87RYIUBHCQ75O674578O45348C67',
    //       scope: 'openid',
    //       idToken: '45hj44,hn6j5h6j5h69857nv56747589 v567n4i6',
    //       tokenType: 'bearer',
    //       expiresIn: 3600
    //     }
    //   });
    // }

  }
}

export const fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
