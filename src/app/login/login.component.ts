import { Component, OnInit, OnDestroy } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { TokenModel } from '../services/authentication/models/token.model';
import { tap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError, Subscription } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  // tslint:disable-next-line: no-inferrable-types
  submitted: boolean = false;
  errorLogin: boolean;
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.initializeLoginForm();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  initializeLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }
    this.errorLogin = false;
    const email: string = this.loginForm.get('email').value;
    const password: string = this.loginForm.get('password').value;
    const authSubs: Subscription = this.authenticationService.authenticate(email, password)
    .pipe(
      tap(() => this.router.navigate(['/profile'])),
      catchError((error: HttpErrorResponse) => {
        this.errorLogin = true;
        return throwError(error);
      })
    ).subscribe();

    this.subscriptions.push(authSubs);

  }

}
