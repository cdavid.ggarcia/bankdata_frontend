import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from '../login/login.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./profile-info/profile-info.module').then(m => m.ProfileInfoModule),
        data: { showHeader: true }
      },
      {
        path: 'edit',
        children: [
          {
            path: 'bank-data',
            loadChildren: () => import('./profile-edit-bank-data/profile-edit-bank-data.module').then(m => m.ProfileEditBankDataModule),
            data: { showHeader: true }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
