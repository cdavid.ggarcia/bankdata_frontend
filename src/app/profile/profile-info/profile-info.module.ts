import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileInfoRoutingModule } from './profile-info-routing.module';
import { ProfileInfoComponent } from './profile-info.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    ProfileInfoComponent
  ],
  imports: [
    CommonModule,
    ProfileInfoRoutingModule,
    SharedModule
  ]
})
export class ProfileInfoModule { }
