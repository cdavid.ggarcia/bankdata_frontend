import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { TokenDecodedInterface } from 'src/app/services/authentication/interfaces/token-decoded.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html'
})
export class ProfileInfoComponent implements OnInit {
  userDecoded: TokenDecodedInterface;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.userDecoded = this.authenticationService.getUserDecoded();
  }

}
