import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileEditBankDataComponent } from './profile-edit-bank-data.component';
import { ProfileEditBankDataRoutingModule } from './profile-edit-bank-data-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    ProfileEditBankDataComponent
  ],
  imports: [
    CommonModule,
    ProfileEditBankDataRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    ProfileEditBankDataComponent
  ]
})
export class ProfileEditBankDataModule { }
