import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileEditBankDataComponent } from './profile-edit-bank-data.component';

describe('ProfileEditBankDataComponent', () => {
  let component: ProfileEditBankDataComponent;
  let fixture: ComponentFixture<ProfileEditBankDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileEditBankDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileEditBankDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
