import { Routes, RouterModule } from '@angular/router';
import { ProfileEditBankDataComponent } from './profile-edit-bank-data.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: ProfileEditBankDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileEditBankDataRoutingModule { }
