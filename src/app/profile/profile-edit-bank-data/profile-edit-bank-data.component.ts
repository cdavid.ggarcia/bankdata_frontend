import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BankDataModel } from 'src/app/services/bank-data/models/bank-data.model';
import { BankDataService } from 'src/app/services/bank-data/bank-data.service';
import { Subscription, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-profile-edit-bank-data',
  templateUrl: './profile-edit-bank-data.component.html'
})
export class ProfileEditBankDataComponent implements OnInit, OnDestroy {

  bankDataForm: FormGroup;
  // tslint:disable-next-line: no-inferrable-types
  submitted: boolean = false;
  errorUpdate: boolean;
  updatedData: boolean;

  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private bankDataService: BankDataService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.getBankData();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
  }

  getBankData(): void {
    this.errorUpdate = false;
    const email: string = this.authenticationService.getUserDecoded().user.email;
    const bankDataSubs: Subscription = this.bankDataService.getBankData(email)
    .pipe(
      tap((model: BankDataModel) => this.initializeLoginForm(model)),
      catchError((error: HttpErrorResponse) => {
        this.errorUpdate = true;
        return throwError(error);
      })
    ).subscribe();

    this.subscriptions.push(bankDataSubs);
  }

  initializeLoginForm(model: BankDataModel): void {
    this.bankDataForm = this.formBuilder.group({
      dni: [model.dni, []],
      iban: [model.iban, [
          Validators.required,
          Validators.pattern('^([A-Z]{2}[ \-]?[0-9]{2})(?=(?:[ \-]?[A-Z0-9]){9,30}$)((?:[ \-]?[A-Z0-9]{3,5}){2,7})([ \-]?[A-Z0-9]{1,3})?$')
        ]],
      address: [model.address, []],
      city: [model.city, []],
      zipCode: [model.zipCode, []]
    });
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.bankDataForm.invalid) {
      return;
    }

    this.errorUpdate = false;
    this.updatedData = false;
    const bankData: BankDataModel = new BankDataModel(this.bankDataForm.getRawValue());
    bankData.email = this.authenticationService.getUserDecoded().user.email;
    const bankDataSubs: Subscription = this.bankDataService.setBankData(bankData)
    .pipe(
      tap(() => this.updatedData = true),
      catchError((error: HttpErrorResponse) => {
        this.errorUpdate = true;
        return throwError(error);
      })
    ).subscribe();

    this.subscriptions.push(bankDataSubs);
  }

}
