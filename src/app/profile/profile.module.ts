import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileEditBankDataComponent } from './profile-edit-bank-data/profile-edit-bank-data.component';
import { ProfileRoutingModule } from './profile.routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProfileRoutingModule
  ]
})
export class ProfileModule { }
