import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
{
  path: '',
  canActivate: [AuthGuard],
  loadChildren: () => import('./main/main.module').then(m => m.MainModule ),
  data: { showHeader: true }
},
{
  path: 'login',
  loadChildren: () => import('./login/login.module').then(m => m.LoginModule ),
  data: { showHeader: false }
},
{
  path: 'logout',
  loadChildren: () => import('./login/login.module').then(m => m.LoginModule ),
  data: { showHeader: false }
},
{
  path: '**',
  loadChildren: () => import('./page-not-found/page-not-found.module').then(m => m.PageNotFoundModule ),
  data: { showHeader: false }
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
