export interface ItemStorageInterface {
  key: string;
  value: string;
  type?: string;
}
