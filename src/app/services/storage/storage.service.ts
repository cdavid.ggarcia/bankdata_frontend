import { Injectable } from '@angular/core';
import { ItemStorageInterface } from './interfaces/item-storage.interface';
import { LOCAL_STORAGE } from '../../constants/storage.constants';


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setItem(key: string, value: string, type: string = LOCAL_STORAGE): void {
    if (type === LOCAL_STORAGE) {
      localStorage.setItem(key, value);
      return;
    }
    sessionStorage.setItem(key, value);
  }

  setItems(items: ItemStorageInterface[]): void {
    items.forEach((x: any) => this.setItem(x.key, x.value, x.type));
  }

  getItem(key: string, type: string = LOCAL_STORAGE): any {
    if (type === LOCAL_STORAGE) {
      return localStorage.getItem(key);
    }
    return sessionStorage.getItem(key);
  }

  removeItem(key: string, type: string = LOCAL_STORAGE): void {
    if (type === LOCAL_STORAGE) {
      localStorage.removeItem(key);
      return;
    }
    sessionStorage.removeItem(key);
  }

}
