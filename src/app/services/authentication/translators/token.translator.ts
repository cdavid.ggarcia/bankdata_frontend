import { TokenContract } from '../../../api/contracts/token.contract';
import { TokenModel } from '../models/token.model';


export class TokenTranslator {

  static translateContractToModel(contract: TokenContract): TokenModel {
    const model: TokenModel = {
      accessToken: (contract && contract.accessToken) ? contract.accessToken : '',
      refreshToken: (contract && contract.refreshToken) ? contract.refreshToken : '',
      scope: (contract && contract.scope) ? contract.scope : '',
      idToken: (contract && contract.idToken) ? contract.idToken : '',
      tokenType: (contract && contract.tokenType) ? contract.tokenType : '',
      expiresIn: (contract && contract.expiresIn) ? contract.expiresIn : null
    };
    return new TokenModel(model);
  }

}
