import { UserDecodedInterface } from './user-decoded.interface';

export interface TokenDecodedInterface {
  iat: number;
  exp: number;
  user: UserDecodedInterface;
}
