export interface UserDecodedInterface {
  email: string;
  name: string;
  surname: string;
  phone: string;
}
