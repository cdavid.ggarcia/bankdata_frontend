export class TokenModel {
  accessToken: string;
  refreshToken: string;
  scope: string;
  idToken: string;
  tokenType: string;
  expiresIn: number;

  constructor(model?: TokenModel) {
    this.accessToken = (model && model.accessToken) ? model.accessToken : '';
    this.refreshToken = (model && model.refreshToken) ? model.refreshToken : '';
    this.scope = (model && model.scope) ? model.scope : '';
    this.idToken = (model && model.idToken) ? model.idToken : '';
    this.tokenType = (model && model.tokenType) ? model.tokenType : '';
    this.expiresIn = (model && model.expiresIn) ? model.expiresIn : null;
  }

}
