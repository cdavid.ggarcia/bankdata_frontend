import { Injectable } from '@angular/core';
import { TokenModel } from './models/token.model';
import { Observable } from 'rxjs';
import { ApiAuthenticationService } from 'src/app/api/services/api-authentication/api-authentication.service';
import { TokenContract } from 'src/app/api/contracts/token.contract';
import { TokenTranslator } from './translators/token.translator';
import { ACCESS_TOKEN } from 'src/app/constants/auth.constants';
import { StorageService } from '../storage/storage.service';
import { map, tap } from 'rxjs/operators';
import { TokenDecodedInterface } from './interfaces/token-decoded.interface';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  jwtHelper = new JwtHelperService();

  constructor(
    private apiAuthenticateService: ApiAuthenticationService,
    private storageService: StorageService
  ) { }

  authenticate(email: string, password: string): Observable<TokenModel> {
    return this.apiAuthenticateService.authenticate(email, password)
      .pipe(
        map((tokenContract: TokenContract) => TokenTranslator.translateContractToModel(tokenContract)),
        tap((tokenModel: TokenModel) => this.storageService.setItem(ACCESS_TOKEN, tokenModel.accessToken)),
      );
  }

  logout(): any {
    return this.storageService.removeItem(ACCESS_TOKEN);
  }


  getUserDecoded(): TokenDecodedInterface {
    const authToken = this.storageService.getItem(ACCESS_TOKEN);
    return this.jwtHelper.decodeToken(authToken);
  }
}
