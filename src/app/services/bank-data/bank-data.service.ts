import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BankDataModel } from './models/bank-data.model';
import { ApiBankDataService } from 'src/app/api/services/api-bank-data/api-bank-data.service';
import { BankDataContract } from 'src/app/api/contracts/bank-data.contract';
import { BankDataTranslator } from './translators/bank-data.translator';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BankDataService {

  constructor(
    private apiBankDataService: ApiBankDataService
  ) { }

  setBankData(bankData: BankDataModel): Observable<boolean> {
    return this.apiBankDataService.setBankData(BankDataTranslator.translateModelToContract(bankData));
  }

  getBankData(email: string): Observable<BankDataModel> {
    return this.apiBankDataService.getBankData(email)
      .pipe(
        map((contract: BankDataContract) => BankDataTranslator.translateContractToModel(contract))
      );
  }
}
