export class BankDataModel {
  email: string;
  dni: string;
  iban: string;
  address: string;
  city: string;
  zipCode: string;

  constructor(model?: BankDataModel) {
    this.email = (model && model.email) ? model.email : '';
    this.dni = (model && model.dni) ? model.dni : '';
    this.iban = (model && model.iban) ? model.iban : '';
    this.address = (model && model.address) ? model.address : '';
    this.city = (model && model.city) ? model.city : '';
    this.zipCode = (model && model.zipCode) ? model.zipCode : '';
  }
}
