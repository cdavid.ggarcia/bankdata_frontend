import { BankDataModel } from '../models/bank-data.model';
import { BankDataContract } from 'src/app/api/contracts/bank-data.contract';

export class BankDataTranslator {
  static translateModelToContract(model: BankDataModel): BankDataContract {
    const contract: BankDataContract = {
      email: (model && model.email) ? model.email : null,
      dni: (model && model.dni) ? model.dni : null,
      iban: (model && model.iban) ? model.iban : null,
      address: (model && model.address) ? model.address : null,
      city: (model && model.city) ? model.city : null,
      zipCode: (model && model.zipCode) ? model.zipCode : null
    };
    return contract;
  }

  static translateContractToModel(contract: BankDataContract): BankDataModel {
    const model: BankDataModel = {
      email: (contract && contract.email) ? contract.email : null,
      dni: (contract && contract.dni) ? contract.dni : null,
      iban: (contract && contract.iban) ? contract.iban : null,
      address: (contract && contract.address) ? contract.address : null,
      city: (contract && contract.city) ? contract.city : null,
      zipCode: (contract && contract.zipCode) ? contract.zipCode : null
    };
    return model;
  }
}
