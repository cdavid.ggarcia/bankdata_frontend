import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { TokenDecodedInterface } from 'src/app/services/authentication/interfaces/token-decoded.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  userDecoded: TokenDecodedInterface;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.userDecoded = this.authenticationService.getUserDecoded();
  }

  logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['/logout']);
  }
}
