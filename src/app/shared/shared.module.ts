import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent , FooterComponent} from './index';
import { RouterModule } from '@angular/router';
import { MessageFormValidationComponent } from './message-form-validation/message-form-validation.component';
import { CardComponent } from './card/card.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MessageFormValidationComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    MessageFormValidationComponent,
    CardComponent
  ]
})
export class SharedModule { }
