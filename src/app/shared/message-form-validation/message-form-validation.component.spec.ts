import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageFormValidationComponent } from './message-form-validation.component';

describe('MessageFormValidationComponent', () => {
  let component: MessageFormValidationComponent;
  let fixture: ComponentFixture<MessageFormValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageFormValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageFormValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
