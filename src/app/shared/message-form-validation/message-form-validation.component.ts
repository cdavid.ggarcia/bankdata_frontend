import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-message-form-validation',
  templateUrl: './message-form-validation.component.html'
})
export class MessageFormValidationComponent implements OnInit {

  @Input() field: string;
  @Input() form: FormGroup;
  @Input() submitted: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
