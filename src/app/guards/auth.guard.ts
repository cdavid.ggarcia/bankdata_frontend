import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { TokenDecodedInterface } from '../services/authentication/interfaces/token-decoded.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  canActivate(): boolean {
    const token: TokenDecodedInterface = this.authenticationService.getUserDecoded();
    if (token && token.user && token.user.email) {
      return true;
    }
    this.router.navigate(['/logout']);
    return false;
  }

}
